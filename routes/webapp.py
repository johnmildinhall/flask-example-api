'''This module exposes the routes to clean NHS SC data'''

from flask import render_template, request, redirect, url_for, jsonify

from routes.aux import example

def expose(app):

    '''expose cleaning routes'''

    @app.route("/", methods=["GET"])
    def home():

        '''return get response'''

        return jsonify({"response":"hello world"})

    @app.route("/add-up", methods=["GET"])
    def add_up():

        '''return get response'''

        int1 = int(request.args.get('int1'))
        int2 = int(request.args.get('int2'))

        result = example.sum(int1, int2)

        return jsonify({"answer":result})


    @app.route("/page", methods=["GET"])
    def upload_page():

        '''return page'''

        return render_template('upload.html')

    @app.route("/clean", methods=["POST"])
    def clean_data():

        '''return post example'''

        print("hello world")

        data = request.json

        return jsonify(data)
