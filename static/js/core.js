function onLoad(){
  button = document.querySelector('button.upload');
  final = document.querySelector('button.final');

  // validate
  button.onclick = function(){
    upload("validate")
  }

  // finalise
  final.onclick = function(){
    upload("final")
  }
}

function upload(uploadType){
  console.log(uploadType)
  var textarea = document.getElementsByClassName('csv-upload')[0].value;
  var payload = JSON.stringify({upload_type:uploadType, csv:textarea});
  var spinner = document.querySelector("."+uploadType+"-spinner");
  // console.log(spinner);
  spinner.className = spinner.className.replace(" hide", "");
  // console.log(payload);
  var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
  xmlhttp.open("POST", "/upload/pasted");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
        console.log(xmlhttp.responseText)
        var data = JSON.parse(xmlhttp.responseText);
        handleResponse(data);
        spinner.className = spinner.className + " hide";
    }
  };
  xmlhttp.send(payload);
}

function handleResponse(data){

  // if final update
  console.log(data)
  if(typeof data.success != 'undefined'){
    console.log("success");
    var table = document.querySelector(".clean-results")
    table.parentNode.removeChild(table);
    final.parentNode.removeChild(final);
    message = document.querySelector(".results-message")
    message.className = message.className.replace(" hide", "")
  }else{ // if validate update
    data = data.data;
    var column_names = ["supplier", "description_1", "uom", "mpc", "quantity"];
    var table = document.querySelector('tbody.results');
    var proto = document.querySelector('tr#proto');

    // loop through each item, create a row in the table
    for ( var i in data ){
      var newRow = proto.cloneNode(true);

      // add each column name
      for ( var j in column_names ){

        var cell = document.createElement("td");

        switch(column_names[j]){
          case "uom":
            if(data[i].uom_exceptions.number > 0){
              var warning = createWarning("exception", data[i].uom_exceptions)
              cell.appendChild(warning)
            }
            break;
          case "supplier":
            if(data[i].supplier_exceptions.number > 0){
              var warning = createWarning("exception", data[i].supplier_exceptions)
              cell.appendChild(warning)
            }
            break;
          case "mpc":
            // console.log(data[i].mpc_exists)
            if(data[i].mpc_exists == true){
              // console.log(data[i])
              var warning = createWarning("tick", '')
              cell.appendChild(warning)
            }
            break;
        }
        var contents = document.createTextNode(data[i][column_names[j]]);
        cell.appendChild(contents);
        cell.className = "mdl-data-table__cell--non-numeric";
        newRow.appendChild(cell);
      }
      table.insertBefore(newRow, proto);

      // show results table
      var resultsTable = document.querySelector('.clean-results');
      resultsTable.className = resultsTable.className.replace('hide','');
      //hide upload textarea
      var uploadArea = document.querySelector('.csv-paste');
    }
    uploadArea.className = uploadArea.className+' hide';
  }
}

function createWarning(type, warningObject){
    var warning = document.createElement("div")
    switch(type){
      case "tick":
        warning.className = "warning tick";
        warning.setAttribute("data-tippy-content","MPC found in catalogue")
        tippy(warning, {arrow:true})
        break;
      case "exception":
        warning.className = "warning exception";

        // add original string
        var string = "<div>Original: "+ warningObject.original+"</div>" ;

        // add standardised string, if it is present
        if (typeof warningObject.standardised != 'undefined'){
          var string = string + "<div>Standardised: "+ warningObject.standardised+"</div>" ;
        }

        // add all the exceptions
        for ( var i = 0 ; i < warningObject.data.length ; i++ ){
          if ( i > 0 ){ var comma = ", "}else{ var comma = ""};
          string = string + comma + warningObject.data[i].description;
        }

        // define tooltip
        warning.setAttribute("data-tippy-content",string)
        tippy(warning, {arrow:true})

        break;
      default:
        warning.className = "warning exception";
        break;
    }

    return warning
}

// console.log("start");
