'''Main app'''

from flask import Flask
from routes import webapp

def create_app():

    '''main app function'''

    app = Flask(__name__)
    webapp.expose(app)
    return app


if __name__ == '__main__':
    APP = create_app()
    APP.run(debug=True)
